﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IntroMvcProject.Models;

namespace IntroMvcProject.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index(int? id)
        {
            return View();
        }

        public ActionResult About(int? id)
        {
            return View();
        }    

        public ActionResult add(Model m)
        {
            ViewBag.add = m.num1 += m.num2;
            return View();
        }

        public ActionResult concat(Model m)
        {
            ViewBag.concat  = m.text1 + m.text2;
            return View();
        }

    }
}
