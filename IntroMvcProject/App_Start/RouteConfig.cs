﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IntroMvcProject
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "About",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "About", id = UrlParameter.Optional },
               constraints: new { id = @"^[0-9]+$" }

           );

            routes.MapRoute(
                name: "Add",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "add" }
            );

            routes.MapRoute(
                name: "Concat",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "concat" }
            );
        }
    }
}